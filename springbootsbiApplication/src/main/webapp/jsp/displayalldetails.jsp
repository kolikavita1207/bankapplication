
<%@page import="com.sbi.fixdeposite.model.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>All the Details</title>
</head>
<body style="background-color:orange;">
	<h2 align="center">FixDeposite  Data</h2>

	<table border="1" align="center">
		<tr>
			<th>custMobileNumber</th>
			<th>custPanNumber</th>
			<th>custName</th>
			<th>custAddress</th>
			<th>custAdharNumber</th>
			<th>fixDepositeAmmount</th>
			<th>fixDepositeDuration</th>
			<th>interestRate</th>
		</tr>
		 <tr>
			<%
			fixDepositeModel fixdepositemodel = (fixDepositeModel)request.getAttribute("logindata");
			%>
			<td><%=fixdepositemodel.getCustMobileNumber()%></td>
			<td><%=fixdepositemodel.getCustPanNumber()%></td>
			<td><%=fixdepositemodel.getCustName()%></td>
			<td><%=fixdepositemodel.getCustAddress()%></td>
			<td><%=fixdepositemodel.getCustAdharNumber()%></td>
			<td><%=fixdepositemodel.getFixDepositeAmmount()%></td>
			<td><%=fixdepositemodel.getFixDepositeDuration()%></td>
			<td><%=fixdepositemodel.getInterestRate()%></td>
		</tr> 

	</table>

	<a href="/">Back</a>
</body>
</html>