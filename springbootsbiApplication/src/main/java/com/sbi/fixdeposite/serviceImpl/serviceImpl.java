package com.sbi.fixdeposite.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbi.fixdeposite.model.fixDepositeModel;
import com.sbi.fixdeposite.repository.Homerepository;
import com.sbi.fixdeposite.service.fixDepositeService;

@Service
public class serviceImpl implements fixDepositeService {
	@Autowired
	Homerepository homerepository;

	public fixDepositeModel savefixDepositeModel(fixDepositeModel custMobileNumber) {
		fixDepositeModel depositeModel = homerepository.save(custMobileNumber);
		return depositeModel;
	}

	public fixDepositeModel getfixDepositeModel(int custMobileNumber) {
		fixDepositeModel depositeModel = homerepository.findBycustMobileNumber(custMobileNumber);
		return depositeModel;
	}

	public void deletefixDepositeModel(int custMobileNumber) {
		homerepository.deleteBycustMobileNumber(custMobileNumber);

	}

	public void updatefixDepositeModel(fixDepositeModel custMobileNumber) {
		homerepository.save(custMobileNumber);

	}

}
