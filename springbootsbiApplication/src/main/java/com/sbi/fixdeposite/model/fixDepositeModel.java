package com.sbi.fixdeposite.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class fixDepositeModel {
	@Id
	private int custMobileNumber;
	private int custPanNumber;
	private String custName;
	private String custAddress;
	private int custAdharNumber;
	private int fixDepositeAmmount;
	private int  fixDepositeDuration;
	private int interestRate;
	public int getCustMobileNumber() {
		return custMobileNumber;
	}
	public void setCustMobileNumber(int custMobileNumber) {
		this.custMobileNumber = custMobileNumber;
	}
	public int getCustPanNumber() {
		return custPanNumber;
	}
	public void setCustPanNumber(int custPanNumber) {
		this.custPanNumber = custPanNumber;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustAddress() {
		return custAddress;
	}
	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}
	public int getCustAdharNumber() {
		return custAdharNumber;
	}
	public void setCustAdharNumber(int custAdharNumber) {
		this.custAdharNumber = custAdharNumber;
	}
	public int getFixDepositeAmmount() {
		return fixDepositeAmmount;
	}
	public void setFixDepositeAmmount(int fixDepositeAmmount) {
		this.fixDepositeAmmount = fixDepositeAmmount;
	}
	public int getFixDepositeDuration() {
		return fixDepositeDuration;
	}
	public void setFixDepositeDuration(int fixDepositeDuration) {
		this.fixDepositeDuration = fixDepositeDuration;
	}
	public int getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(int interestRate) {
		this.interestRate = interestRate;
	}
	
}
