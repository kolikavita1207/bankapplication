package com.sbi.fixdeposite.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sbi.fixdeposite.model.fixDepositeModel;
import com.sbi.fixdeposite.service.fixDepositeService;

@Controller
public class fixdepositeContoller {
	@Autowired
	fixDepositeService fixdepositeservice;

	@RequestMapping("/")
	public String fixdeposite() {
		return "fixDepositelogin";
	}

	@RequestMapping("fixDepositelog")
	public String fixDepositepage(ModelMap model, @RequestParam String custMobileNumber, @RequestParam String custName,
			Model m) {

		if (custMobileNumber.equalsIgnoreCase("1234") && custName.equalsIgnoreCase("kavita")) {
			fixDepositeModel fixdepositemodel = fixdepositeservice.getfixDepositeModel(1234);
			m.addAttribute("logindata", fixdepositemodel);
			return "displayalldetails";

		} else {
			model.put("errormsg", "please provide the correct id and password");
			return "fixDepositelogin";
		}
	}

	@RequestMapping("fixDepositeDetails")
	public String fixdepositedetal() {
		return "fixDepositeDetails";
	}

	@RequestMapping("Details")
	public String custDetail(ModelMap model, @RequestParam String custMobileNumber, @RequestParam String custPanNumber,
			@RequestParam String custName, @RequestParam String custAddress, @RequestParam String custAdharNumber,
			@RequestParam String fixDepositeAmmount, @RequestParam String fixDepositeDuration,
			@RequestParam String interestRate) {
		fixDepositeModel fixdepositemodel = new fixDepositeModel();
		fixdepositemodel.setCustMobileNumber(Integer.parseInt(custMobileNumber));
		fixdepositemodel.setCustPanNumber(Integer.parseInt(custPanNumber));
		fixdepositemodel.setCustName(custName);
		fixdepositemodel.setCustAddress(custAddress);
		fixdepositemodel.setCustAdharNumber(Integer.parseInt(custAdharNumber));
		fixdepositemodel.setFixDepositeAmmount(Integer.parseInt(fixDepositeAmmount));
		fixdepositemodel.setFixDepositeDuration(Integer.parseInt(fixDepositeDuration));
		fixdepositemodel.setInterestRate(Integer.parseInt(interestRate));
		fixdepositeservice.savefixDepositeModel(fixdepositemodel);
		model.put("successmsg", "register Successfully..........");

		return "fixDepositelogin";

	}

	@RequestMapping("delete")
	public String deletefixDepositeModel() {
		fixdepositeservice.deletefixDepositeModel(1234);
		return "fixDepositelogin";

	}

	@RequestMapping("update")
	public String updatefixDepositeModel() {
		fixDepositeModel fixdepositemodel = new fixDepositeModel();
		fixdepositemodel.setCustMobileNumber(34455);
		fixdepositemodel.setCustPanNumber(234567);
		fixdepositemodel.setCustName("rani");
		fixdepositemodel.setCustAddress("raychur");
		fixdepositemodel.setCustAdharNumber(1234566);
		fixdepositemodel.setFixDepositeAmmount(200000);
		fixdepositemodel.setFixDepositeDuration(2);
		fixdepositemodel.setInterestRate(9);
		fixdepositeservice.updatefixDepositeModel(fixdepositemodel);

		return "fixDepositelogin";

	}
}
