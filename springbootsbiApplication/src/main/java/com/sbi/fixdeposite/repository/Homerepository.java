package com.sbi.fixdeposite.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.sbi.fixdeposite.model.fixDepositeModel;
@Repository
@Transactional
public interface Homerepository  extends CrudRepository<fixDepositeModel, Integer>
{
    public fixDepositeModel save(fixDepositeModel custMobileNumber );
	
	public fixDepositeModel findBycustMobileNumber(int custMobileNumber);

	public void deleteBycustMobileNumber(int custMobileNumber);

}
