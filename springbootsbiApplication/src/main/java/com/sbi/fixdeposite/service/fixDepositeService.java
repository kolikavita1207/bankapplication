package com.sbi.fixdeposite.service;

import com.sbi.fixdeposite.model.fixDepositeModel;

public interface fixDepositeService {

	public fixDepositeModel savefixDepositeModel(fixDepositeModel custMobileNumber);

	public fixDepositeModel getfixDepositeModel(int custMobileNumber);

	public void deletefixDepositeModel(int custMobileNumber);

	public void updatefixDepositeModel(fixDepositeModel custMobileNumber);

}
