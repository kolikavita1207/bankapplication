package com.sbi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootsbiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootsbiApplication.class, args);
	}

}
